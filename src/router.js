import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Cadastro from "./views/Cadastro.vue";
import Vote from "./views/Vote.vue";
import ResultVote from "./views/ResultVote.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/cadastro",
      name: "cadastro",
      component: Cadastro
    },
    {
      path: "/votar",
      name: "votar",
      component: Vote
    },
    {
      path: "/resultadoVotar",
      name: "resultadoVotar",
      component: ResultVote
    }
  ]
});
