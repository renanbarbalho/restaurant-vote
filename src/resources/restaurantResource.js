import Vue from "vue";
import Resource from "vue-resource";

Vue.use(Resource);

export default {
  geral: Vue.resource("restaurant"),
  deleteRest: Vue.resource("restaurant/{id}"),
  checkVote: Vue.resource("restaurant/openVote")
};
