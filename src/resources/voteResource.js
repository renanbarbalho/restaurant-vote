import Vue from "vue";
import Resource from "vue-resource";

Vue.use(Resource);

export default {
  vote: Vue.resource("vote"),
  listar: Vue.resource("vote/result")
};
