import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Resource from "vue-resource";
import BootstrapVue from "bootstrap-vue";

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

Vue.use(Resource);
Vue.http.options.root = "/restaurantes"; // contexto do backend (atualmente SB)
Vue.http.options.withCredentials = true;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
